FROM python:3.8-alpine

RUN set -ex && apk add --no-cache exiftool

WORKDIR /usr/src/app

COPY . .