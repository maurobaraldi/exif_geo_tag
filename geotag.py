#!/usr/bin/env python

import os
import subprocess


def load_definitions(path):
    '''Load photos definitions from JSON file.'''

    if not os.path.exists(path):
        print("The photos.json file wasn't defined.")
        return False

    with open(path) as f:
        return loads(f.read())

def geo_tag(filename, latitude, longitude):
    '''Tag image file with lat long.'''

    if not os.path.exists(filename):
        print(f"Image file {filename} not found.")
        return False

    with open(os.devnull, "w") as _:
        subprocess.Popen(
            [
                'exiftool',
                f"-exif:gpslatitude={latitude}",
                f"-exif:gpslatituderef={latitude}",
                f"-exif:gpslongitude={longitude}",
                f"-exif:gpslongituderef={longitude}",
                filename
            ],
            stdin=subprocess.PIPE, 
            stdout=subprocess.PIPE,
            stderr=_
        )

    print(f"Image file {filename} exif metadata updated successfuly.")
    return True
